django-countries (6.1.2-1) UNRELEASED; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Michael Fladischer <fladi@debian.org>  Sun, 07 Jun 2020 15:59:06 +0200

django-countries (6.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.

 -- Michael Fladischer <fladi@debian.org>  Thu, 19 Mar 2020 19:08:22 +0100

django-countries (5.3.2-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Wed, 31 Jul 2019 14:33:18 +0200

django-countries (5.3.2-1) unstable; urgency=low

  * New upstream release (Closes: #902458).
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Refresh patches.
  * Bump Standards-Version to 4.2.1.
  * Create symlinks after dh_python3.

 -- Michael Fladischer <fladi@debian.org>  Sat, 22 Sep 2018 18:19:14 +0200

django-countries (5.2-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Michael Fladischer ]
  * New upstream release (Closes: #865790).
  * Always use pristine-tar.
  * Disable PGP signature check in uscan as upstream is not providing a
    signature for current releases.
  * Move upstream signing key to debian/upstream/signing-key.asc.
  * Refresh patches.
  * Add patch to fix tests to work with local XML data from iso-codes
    package.
  * Clean up django_countries.egg-info/requires.txt to allow two builds
    in a row.
  * Install upstream changelog.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Enable autopkgtest-pkg-python testsuite.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Remove lintian override for missing upstream changelog.
  * Change dpkg-triggers to use noawait variants (Closes: #803448).
  * Remove lintian override for empty directory used for symlink farm.

 -- Michael Fladischer <fladi@debian.org>  Sun, 01 Apr 2018 20:02:34 +0200

django-countries (3.4.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ Thomas Goirand ]
  * Add patch to define TEMPLATES which is needed to avoid FTBFS in Django 1.10:
    - 0003-Django-1.10-define-TEMPLATES.patch (Closes: #828646).
  * Standards-Version: 3.9.8 (no change).

 -- Thomas Goirand <zigo@debian.org>  Fri, 05 Aug 2016 12:41:59 +0000

django-countries (3.4.1-2) unstable; urgency=medium

  * Update patch to use iso-codes package data to skip another translation
    test that triggers a false negative during build (Closes: #806347).
  * Remove the whole locales directory in the prerm script (Closes: #803448).

 -- Michael Fladischer <fladi@debian.org>  Sat, 26 Dec 2015 20:24:33 +0100

django-countries (3.4.1-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Drop python3.4-UnicodeDecodeError.patch, fixed upstream.
  * Add python(3)-djangorestframework to Build-Depends as it is now required
    by a test.
  * Add python(3)-djangorestframework to Enhances.
  * Bump debhelper compatibility and version to 9.
  * Bump Standards-Version to 3.9.6.
  * Use pypi.debian.net service for uscan.
  * Change my email address to fladi@debian.org.

  [ SVN-Git Migration ]
  * Initialize git-dpm.
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Fri, 23 Oct 2015 08:57:25 +0200

django-countries (2.1.2-1) unstable; urgency=medium

  * New upstream release (Closes: #755592).
  * Add dh-python to Build-Depends.
  * Switch buildsystem to pybuild.
  * Update patch to use iso-codes package data to new ustream code.
  * Update patch to not include flag icons.
  * Add Python3 support.
  * Add python3.4-UnicodeDecodeError.patch to fix ecoding error when
    reading the README.rst file.
  * Rewrite README.Debian to reflect changes to famfamfam-flag-*
    symlink.
  * Add lintian-overrides missing upstream changelog and symlink to
    famfamfam-flag-*.
  * Check PGP signature on upstream tarball.
  * Update years in d/copyright.
  * Remove superfluous settings.py used for tests, upstream ships its own
    now.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Mon, 01 Sep 2014 15:26:05 +0200

django-countries (1.5-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards version to 3.9.5.
  * README now installs as README.rst.
  * Make Build-Depends on python-all and python-django unversioned as no
    prior versions are available in Wheezy.
  * Drop temporary fix for Metadata-Version in PKG-INFO.
  * Rename MIT license to Expat.
  * Update years in d/copyright.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Mon, 16 Dec 2013 13:10:35 +0100

django-countries (1.2-1) unstable; urgency=low

  * New upstream release.
  * Fix tests for django (>= 1.4) FTBFS (Closes: #669492).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Sat, 21 Apr 2012 22:59:31 +0200

django-countries (1.1.2-1) unstable; urgency=low

  * New upstream release.
  * Make Metadata-Version fix more generic.
  * Bump Standards-Version to 3.9.3.
  * Change order of my name.
  * Update DEP-5 URL to 1.0.
  * Update years in d/copyright.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Fri, 16 Mar 2012 19:17:19 +0100

django-countries (1.1.1-1) unstable; urgency=low

  * New upstream release.
  * Prefer PNG icons.
  * Streamline packaging code with wrap-and-sort.
  * Update d/copyright.
  * Bump standards version to 3.9.2.
  * Update dont_include_flag_icons patch.
  * Fix patch descriptions to match DEP3.
  * Add suffix .patch to all patches.
  * Use Django's lazy translation on country names and symlink iso-
    codes translations to django.mo files in our own directory
    structure (Thanks to Jakub Wilk for his help!).
  * Add lintian override as the locale directory is initially empty and
    populated by postinst.
  * Temporary fix for Metadata-Version in PKG-INFO.
  * Switch to dh_python2.
  * Generate COUNTRIES_PLUS by duplicating countries with a comma in
    their name. The part after the comma is moved to the beginning of the
    string and the comma is removed.
  * Sort countries by name using unicodedata.normalize in NFD mode.
  * Update settings.py to use new DATABASE settings format.
  * Bump build dependency for python-django to >= 1.2.
  * Update flag icons path in d/copyright.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Sat, 19 Nov 2011 18:14:25 +0100

django-countries (1.0.5-1) unstable; urgency=low

  * New upstream release.
  * Bump to standards-version 3.9.1.
  * Update DEP5 format.
  * Drop debian/pyversions in favour of XS-Python-Version.
  * Simplify debian/rules by using a predefined settings.py.
  * Set PMPT as maintainer and myself as uploader.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Mon, 07 Feb 2011 13:24:36 +0100

django-countries (1.0.4-1) unstable; urgency=low

  * New upstream release.
  * Use DEP5 for copyright file.
  * Update to Debian policy 3.9.0.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Wed, 07 Jul 2010 12:38:20 +0200

django-countries (1.0.3-1) unstable; urgency=low

  * Initial release (Closes: #576262)

 -- Fladischer Michael <FladischerMichael@fladi.at>  Fri, 23 Apr 2010 14:45:20 +0200
